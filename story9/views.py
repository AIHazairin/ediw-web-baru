from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate, logout
# Create your views here.

def index(request):
	return render(request, 'story9/story9.html')


def signup(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('story9:index')
	else:
		form = UserCreationForm()
	return render(request, 'story9/signup.html', {'form': form})

def logging_in(request):
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = authenticate(username=username, password=password)
		login(request, user)
		return redirect('story9:index')
	else:
		return render(request, 'story9/login.html')

def logging_out(request):
	logout(request)
	return redirect('story9:index')
