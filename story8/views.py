from django.shortcuts import render
from django.http import JsonResponse
import requests, json

# Create your views here.

def index(request):
	return render(request, 'story8/story8.html')

def jsoncall(request):
	url = 'https://www.googleapis.com/books/v1/volumes?q=' + request.GET['q']
	ret = requests.get(url)
	# print(url)
	# print(ret)
	data = json.loads(ret.content)
	return JsonResponse(data, safe=False)