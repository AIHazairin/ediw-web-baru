from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest, response
from django.apps import apps
from story8.apps import Story8Config

# Create your tests here.
class TestViews(TestCase):
	def test_url_sudah_benar(self):
		response = Client().get(reverse("story8:index"))
		self.assertEquals(response.status_code, 200)

	def test_template_sudah_benar(self):
		response = Client().get(reverse("story8:index"))
		self.assertTemplateUsed(response, "story8/story8.html")

	def test_isi_sudah_betul(self):
		response = Client().get(reverse("story8:index"))
		self.assertContains(response, "looking for books?")

	def test_url_untuk_mencari_buku_bisa_diakses(self):
		response = Client().get('/story8/search/?q=thereisnobookwiththisname')
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.json(), {"kind": "books#volumes", "totalItems": 0})

class AppsTest(TestCase):
	def test_apps(self):
		self.assertEqual(Story8Config.name, 'story8')
		self.assertEqual(apps.get_app_config('story8').name, 'story8')
		