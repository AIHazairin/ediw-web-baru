$(document).ready(function() {

	$("#search").keyup(function() {
		var input = $("#search").val();
		// console.log(input);
		// panggil dengan teknik AJAX
		$.ajax({
			url: '/story8/search?q=' + input,
			success : function(data) {
				// console.log(data);
				var array_items = data.items;
				// console.log(array_items);
				// console.log("test");
				$("#list-of-content").empty();
				for (var i = 0; i < array_items.length; i++) {
					gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;
					judul = array_items[i].volumeInfo.title;
					author = array_items[i].volumeInfo.authors != null ? array_items[i].volumeInfo.authors[0] : "Author not included";
					desc = array_items[i].volumeInfo.description != null ? array_items[i].volumeInfo.description : "No description";
					link = array_items[i].volumeInfo.previewLink;
					// console.log(author);
					$("#list-of-content").append(
							"<li><div><div id=\"image\"><img src=" + 
							gambar + 
							"></div><div id=\"detail\"><a href=" + 
							link + 
							" target=\"_blank\"><h4>" +
							judul +
							"</h4></a><hr><h6>" +
							author +
							"</h6><div id=\"desc\"><p>" +
							desc + 
							"</p></div></div></div></li>"
						);
				}
				$("#list-of-content").find("li>div").addClass("content");
				$("#list-of-content").find("li>div #image").addClass("image");
				$("#list-of-content").find("li>div #detail").addClass("detail");
			}
		})
	});
})