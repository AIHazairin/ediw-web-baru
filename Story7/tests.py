from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest, response

# Create your tests here.

class TestViews(TestCase):
	def test_url_sudah_tepat(self):
		response = Client().get(reverse("Story7:index"))
		self.assertEquals(response.status_code, 200)

	def test_template_dan_isinya_sudah_sesuai(self):
		response = Client().get(reverse("Story7:index"))
		self.assertTemplateUsed(response, "home/story7.html")
		self.assertContains(response, "my portofolio in an accordion")