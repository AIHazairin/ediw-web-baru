$(document).ready(function() {

	$('.toggle').click(function(e) {
	  	e.preventDefault();

	    console.log($(this).parent().next());
	    console.log($(this).parent().parent().parent().find('li .inner'));
	  
	    if ($(this).parent().next().hasClass('show')) {
	        $(this).parent().next().removeClass('show');
	        $(this).parent().next().slideUp(350);
	    } else {
	        $(this).parent().parent().parent().find('li .inner').removeClass('show');
	        $(this).parent().parent().parent().find('li .inner').slideUp(350);
	        $(this).parent().next().toggleClass('show');
	        $(this).parent().next().slideToggle(350);
	    }
	});

	$(".down").click(function() {
		var current = $(this).parent().parent();
		var next = current.next()
		current.insertAfter(next);
	});

	$(".up").click(function() {
		let current = $(this).parent().parent();
		let prev = current.prev()
		current.insertBefore(prev);
	})

})