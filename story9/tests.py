from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest, response
from django.apps import apps
from story9.apps import Story9Config
# Create your tests here.

class TestURLandTemplate(TestCase):
	def test_url_sudah_benar(self):
		response = Client().get(reverse("story9:index"))
		self.assertEquals(response.status_code, 200)

	def test_template_sudah_benar(self):
		response = Client().get(reverse("story9:index"))
		self.assertTemplateUsed(response, "story9/story9.html")

	def test_isi_sudah_betul(self):
		response = Client().get(reverse("story9:index"))
		self.assertContains(response, "Belum punya akun?")

class ViewsTest(TestCase):
	def test_sign_up(self):
		signup_data = {
			"username" : "test",
			"password1" : "testtest123", 
			"password2" : "testtest123",
		}
		response = Client().post(reverse("story9:signup"), signup_data)
		self.assertEquals(response.status_code, 302)

	def test_login_and_logout(self):
		signup_data = {
			'username' : "test",
			"password1" : "testtest123", 
			"password2" : "testtest123",
		}
		Client().post(reverse('story9:signup'), signup_data)

		response = Client().post(reverse("story9:login"), {'username':"test", 'password':"testtest123"})
		self.assertEquals(response.status_code, 302)
		
		response1 = Client().get(reverse("story9:logout"))
		self.assertEquals(response1.status_code, 302)

class AppsTest(TestCase):
	def test_apps(self):
		self.assertEqual(Story9Config.name, 'story9')
		self.assertEqual(apps.get_app_config('story9').name, 'story9')